create table frec_apellidos as 
select ROW_NUMBER() OVER (order by apellido) as ap_id
,apellido, sum(frecuencia) as frecuencia
FROM
(select a_paterno as apellido,
	   count(a_paterno) as frecuencia
from nombres_raw
	group by a_paterno
	having frecuencia > 5
union all 
select a_materno as apellido,
	   count(a_materno) as frecuencia
from nombres_raw
	group by a_materno
	having frecuencia > 5
) aps
group by apellido
order by frecuencia desc;

create table frec_nombres as 
select 
ROW_NUMBER() OVER (ORDER BY nombre) as nom_id
,nombre
,count(nombre) as frecuencia
from nombres_raw
	group by nombre
	having frecuencia > 5
