#!/usr/bin/env python3
"""
Synthdata is a python project to help build fake data for learning and testing
data science and numerical methods algorithms.
"""

import numpy as np
import pandas as pd
import sqlite3 as sql


def line_from_points(n_points, interval, point1, point2):
    """
    Univariate linear regression without noise
    creates data using points to determine the
    line parameters and interpolate inside using
    a degree 1 polynomial approximation
    in the interval [interval[0], interval[1]]
    inputs:
    n_points: integer, number of points to generate
    interval: tuple of floats, min and max values of x
    point1: tuple of floats representing point 1
    point2:  tuple of floats representing point 2
    outputs:
    dataframe of points with columns x, y
    """

    m = (point1[1] - point2[1]) / (point1[0] - point1[0])
    b = point1[1] / m * point1[0]
    df = line_from_slope(n_points, m, b, interval)
    return df


def line_from_slope(n_points, slope, intercept, interval):
    """
    Univariate linear regression without noise
    creates data using the line equation y = mx + b
    in the interval [interval[0], interval[1]]
    inputs:
    n_points: integer, number of points to generate
    slope: float, slope of the line
    intercept: float, intercept of the line
    interval: tuple of floats, min and max values of x
    outputs:
    dataframe of points with columns x, y
    """
    x = np.linspace(interval[0], interval[1], n_points)
    retdf = pd.DataFrame(x, columns=['x'])
    retdf['y'] = (slope * retdf['x']) + intercept
    return retdf


def linear_regression_w_noise(method, n_points, slope, intercept, sigma,
                              interval, x, y):
    """
    Univariate linear regression
    creates data using the equation y = mx + b + e
    in the interval [interval[0], interval[1]]
    inputs:
    n_points: integer, number of points to generate
    slope: float, slope of the line
    intercept: float, intercept of the line
    sigma: the standard deviation of the random noise
    interval: tuple of floats, min and max values of x
    outputs:
    dataframe of points with columns x, y
    """
    if method == "slope":
        retdf = line_from_slope(n_points, slope, intercept, interval)
    elif method == "points":
        retdf = line_from_points(n_points, interval, x, y)
    e = np.random.normal(0, sigma, n_points)
    retdf['y'] = retdf['y'] + e
    return retdf


def time_series_generator(series_type, num_points, interval,
                          drift_params, num_waves, wave_params, sigma):
    """
    A time series generator, it can generate additive or multiplicative time
    series, with noise and drift, and an arbitrary number of parts.
    The result is a dataframe with a series of values for the series.
    """
    df = line_from_slope(num_points, drift_params[0], drift_params[1],
                         interval)
    df['e'] = np.random.normal(0, sigma, num_points)
    for i in range(0, num_waves):
        col_name = 'w_'+str(i)
        # revisar si va sobre pi la X
    cols_to_sum = df.columns[: df.shape[1]-1]  # drop the x column
    df[col_name] = wave_params[i][0] * np.sin(df["x"] + wave_params[i][1]) \
        + wave_params[i][2]
    if series_type == 'additive':
        df['ts'] = cols_to_sum.sum(axis=1)
    elif series_type == 'multiplicative':
        df['ts'] = cols_to_sum.multiply(axis=1)
    return df


def gen_pop_segments(factors, frequencies):
    """
    gen_pop_segments: generates a sample population of factors given their
    frequencies both arguments are numpy arrays, and factors can be a 2
    dimensional array where each row is a segment.
    inputs:
         factors: numpy array (1 or 2 dimensional)
         frequencies: numpy array
    note: Both arrays must be the same length.
    output: numpy array of the segments.
    if you want to generate a discrete variable (like age) and not generate
    age factors do it in a different step.
    """
    result = []
    it = np.nditer(frequencies, flags=['f_index'])
    for x in it:
        sam = np.tile(factors[it.index], (x, 1))
        result.append(sam)
    sample = np.vstack(result)
    return sample


def generate_names(n, male_freq, fem_freq):
    """
    Name generation routine it generates n names with the given male and female
    frequencies, using mexican names taken from different sources.
    """
    with open()
        # select name, gender from encarte group by name, gender
    # having count(name) >= 5
