# synthdata-generator

Synthetic data generators using different utilities like faker, and random 
number generators.

## Table of contents

1. Motivation
2. List of generators
3. Theoretical considerations
4. Validations


## Motivation

One of the main problems while learning or practicing data science is the
absence of open data with the required statistical properties to study something
or with the required format. For this, a good combination of random generators
can come handy to the student or practitioner, these generators, generally cover
all or most of the statistical properties desired with the exception of the
relationship between the variables under study as generally, only one variable
at a time can be generated and there is no covariance between generated
variables. For this, a sample population generator has been created and it
generates the desired number of elements following a population segmentation
with a desired abundance. For other uses, a time series generator using either a
multiplicative or additive model has been created and also, a linear univariate
generator with variable noise. 

## List of generators

- Linear regression with noise
- Time series with trend, seasonality, cycles, and random variations (additive
  and multiplicative)
- Sample generation based on segments (discrete and continuous variables)
- Personal data generator (name, sex, age, date of birth, CURP, phone, address)
- Time series generation with a Markov process with drift. 

## Theoretical considerations

The utility of the datasets generated is a widely discussed measure in the
literature. Dataset utility varies depending on what is the purpose of the data
generated. Most synthetic data generators in the market are for software testing
purposes and help validate or test software, meanwhile, synthetic data
generators for machine learning purposes are much more rare as they require a
statistical knowledge of the underlying distributions of the data or detailed
knowledge of a process. 
Here, we use a hybrid approach, we assume that some data is available for
analysis or some information about the process is available and from this
information we generate the data. 

### Case of linear univariate regressions

For linear univariate regression data, we make available two approaches. A
linear interpolation method that needs at least two points. and a
slope-intercept method. 


## Validations

  
